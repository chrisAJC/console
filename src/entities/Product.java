package entities;

public class Product {
	private int id;
	private String description;
	private double prix;
	private String libelle;
	
	public Product(int id, String description, double prix, String libelle) {
		super();
		this.id = id;
		this.description = description;
		this.prix = prix;
		this.libelle = libelle;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", description=" + description + ", prix=" + prix + ", libelle=" + libelle + "]";
	}
	
	
}
