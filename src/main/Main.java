package main;

import javax.xml.ws.*;

import dao.DaoProduct;
import ws.DemoImpl;
import ws.ProductWSImpl;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			
			//Endpoint.publish("http://localhost:4789/ws/demo", new DemoImpl());
			Endpoint.publish("http://localhost:4790/ws/product", new ProductWSImpl());
			System.out.println("Done");
			
		}catch (Exception e) {
			
			System.out.println(e.getMessage());
		}
		
		
		
	}

}
