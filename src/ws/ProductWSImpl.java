package ws;

import java.util.List;

import javax.jws.WebService;

import dao.DaoProduct;
import entities.Product;


@WebService(endpointInterface="ws.ProductI")
public class ProductWSImpl implements ProductI{

	private DaoProduct dao = new DaoProduct();


	@Override
	public Product findById(int id) {
		// TODO Auto-generated method stub
		return this.dao.findById(id);
	}

	@Override
	public List<Product> findAll() {
		// TODO Auto-generated method stub
		return this.dao.findAll();
	}


}

