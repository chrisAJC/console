package ws;

import java.util.*;

import javax.jws.*; // importer le package java web services

import entities.Product;

@WebService
public interface ProductI {
	@WebMethod
	public Product findById(int id);
	
	@WebMethod
	public List<Product> findAll();
}
