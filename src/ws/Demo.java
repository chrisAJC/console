package ws;

import javax.jws.*; // importer le package java web services

@WebService
public interface Demo {
	@WebMethod
	public String helloworld();
	
	@WebMethod
	public String hi(String fullName);

}
